## Cara Penggunaan Simple App BMI
Kirim request dengan value `height` dan `weight` ke domain app ini.
```
curl "http://ec2-54-255-193-54.ap-southeast-1.compute.amazonaws.com/?height=180&weight=70" | jq
```

## Akses Monitoring Dashboard
Monitoring menggunakan cAdvisor, Promtail, Loki, Prometheus dan Grafana
```
Domain : ec2-18-141-240-141.ap-southeast-1.compute.amazonaws.com:3000
IP : 18.141.240.141:3000
Credential sent via email

Login > Dashboards > Manage > Select Any Dashboard
```
## CI/CD Pipeline
Please check it here [CI/CD](https://gitlab.com/takatux/body-mass-index/-/pipelines)

## Design Diagram
![Design Diagram](img/design-diagram-bmi.png)
